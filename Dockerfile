# Build-stage image:
FROM continuumio/miniconda3 AS build

# Create the environment:
COPY environment.yml .
RUN conda env create -f environment.yml

# Install conda-pack:
RUN conda install -c conda-forge conda-pack

# Use conda-pack to create a standalone environment in /venv:
RUN conda-pack -n deep-learning -o /tmp/env.tar && \
  mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
  rm /tmp/env.tar

# Put venv in same path it'll be in final image,
# so now fix up paths:
RUN /venv/bin/conda-unpack

# The runtime-stage image; we can use Debian as the
# base image since the Conda env also includes Python
# for us.
FROM debian:bullseye-20220328-slim AS runtime

# Copy /venv from the previous stage:
COPY --from=build /venv /venv

# Enable venv
ENV PATH="/venv/bin:$PATH"

## The data in our repo only contains public data without features useful
## for deanonymisation. We expect there will be experimentation with the
## process of cleaning and preparing the data, so we leave it in.
## In general, remove it.

## Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini

WORKDIR deep-learning/

ENTRYPOINT ["/usr/bin/tini", "--"]
EXPOSE 8888
CMD ["jupyter", "notebook", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
# Generative Adversarial Networks (GANs)

Creating a Generative Adversarial Network (GAN) to generate a data distribution produced by a sine function, and using deep convolutional GAN to generate an MNIST data distribution.

These notebooks serve us to practice some DL techniques, and as snippets to build on. For now, just the basics for understanding how GANs work.

* [Generating a data distribution from a known function](sine.ipynb)
* [Building a generator network](generator.ipynb)
* [Building a discriminator network](discriminator.ipynb)
* [Building a GAN network](gan.ipynb)

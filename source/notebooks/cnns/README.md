# Convolutional Neural Networks (CNNs)

Using TensorFlow to develop image classifiers, transfer learning and using state-of-the-art algorithms.

These notebooks serve us to practice some DL techniques, and as snippets to build on. Some models are not performing very well. The dataset is too small or the model requires more training. Training a CNN takes a lot of time. Moving on.

- [Wine quality](winequality.ipynb)
- [Churn](churn.ipynb)
- [Recognizing handwritten digits with CNN using Keras](mnist_deep.ipynb)
- [Classifying cats versus dogs with data generators](cats_and_dogs.ipynb)
- [CIFAR10 image classification](cifar.ipynb)


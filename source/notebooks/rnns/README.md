# Recurrent Neural Networks (RNNs)

Recurrent Neural Networks (RNNs) are an intuitive approach to sequence processing. Implementing RNN, 1D convolution, and a hybrid model on a classic sequence processing task – stock price prediction, for comparison.

These notebooks serve us to practice some DL techniques, and as snippets to build on.

- [Forward pass of a Simple RNN](forward_pass.ipynb)
- [Apple stock price prediction](apple.ipynb)
- [IBM stock price prediction](ibm.ipynb)

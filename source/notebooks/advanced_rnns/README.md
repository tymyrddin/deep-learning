# LSTMs, GRUs, and Advanced RNNs

Advanced models and variations of the plain Recurrent Neural Network (RNN) that overcome some RNNs practical drawbacks.

These notebooks serve us to practice some DL techniques, and as snippets to build on.

- [Sentiment classification with RNN](sentiment_rnn.ipynb)
- [LSTM-Based sentiment classification model](sentiment_lstm.ipynb)
- [GRU-Based sentiment classification model](sentiment_gru.ipynb)
- [Bi-directional LSTM-based sentiment classification model](sentiment_bi_lstm.ipynb)
- [Stacked LSTM-based sentiment classification model](sentiment_stacked_lstm.ipynb)

## Training results comparison

| Model               | Epochs  | Training time | Test accuracy |
|---------------------|---------|---------------|---------------|
| RNN                 | 10      | Low           | 0.83          |
| LTSM                | 5       | High          | 0.87          |
| GRU                 | 4       | Medium        | 0.87          |
| Bi-directional LSTM | 4       | Very High     | 0.88          |
| Stacked             | 4       | Very High     | 0.87          |

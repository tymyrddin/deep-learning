# Traditional Neural Networks

Artificial neural networks are inspired by biological neural networks. A simple single-layer neuron is called a perceptron and can easily be implemented it in TensorFlow. Multilayer neural networks can solve more complex multiclass classification tasks. With Keras modular and easy-to-customize neural network models can be built.

These notebooks serve us to practice some DL techniques, and as snippets to build on.

- [Perceptron](perceptron.ipynb)
- [Perceptron as a binary classifier](binary.ipynb)
- [Multiclass classification using a perceptron](multiclass.ipynb)
- [Classifying handwritten digits](mnist.ipynb)
- [Binary classification using Keras](binary_keras.ipynb)
- [Multilayer binary classifier](binary_multilayer.ipynb)
- [Deep neural network on MNIST using Keras](mnist_deep.ipynb)
- [Multilayer neural network to classify sonar signals](sonar_multilayer.ipynb)


## Notes

|                     | Binary classification | Multiclass classification |
|---------------------|-----------------------|---------------------------|
| Number of neurons   | 1                     | Number of classes         |
| Activation function | sigmoid               | softmax                   |


